####################################################################################
# Filename:     main.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   08.04.2021
# Last update:  27.04.2021
#
# Version:      2.0
#
# Filetype:     Main Application file
# Description:  Owlet IoT Message injector Application
# STATUS:       Stable
# Limitation:
####################################################################################

import requests
from requests.auth import HTTPBasicAuth
from g3_d2c_message_builder import *
from g3_c2d_message_builder import *
from csv_devices import *
from coap import *
import eel
import sys
import platform
import threading

selected_c2d_message = ''


def start_eel():
    """
    This Function starts the GUI and keeps Python running in Background
    """

    page = 'index.html'
    eel.init("gui")

    try:
        # Tries to open app in Chrome
        eel.start(page, mode='chrome')
    except EnvironmentError:
        # If Chrome isn't found, fallback to Microsoft Edge on Win10 or greater
        if sys.platform in ['win32', 'win64'] and int(platform.release()) >= 10:
            eel.start(page, mode='edge', block=False)
        else:
            raise

    while True:
        # Loop to keep Python running
        print('\nPython is running....')
        eel.sleep(2)


def rabbit_request(url, request_type, payload=''):
    """
    This function sends the received request to Rabbit MQ
    Returns the received response
    payload is optional, e.g. for GET requests, but mandatory for POST requests
    """

    auth = HTTPBasicAuth(config.rabbit_mq_user, config.rabbit_mq_pass)

    headers = {
        "Accept": "application/json"
    }

    try:
        response = requests.request(
            request_type,
            url,
            headers=headers,
            data=payload,
            auth=auth
        )
    except:
        print("Not possible to establish Connection. Please check Owlet VPN is Running...")
        response = requests.Response()
        return response

    return response


@eel.expose
def validate_rabbit_mq_credentials(gui_username, gui_password):
    """
    This function receives the credentials from GUI user input and generates a Rabbit MQ Request
    Returns True if the Authentication was successful
    Returns False otherwise
    """

    config.rabbit_mq_user = gui_username
    config.rabbit_mq_pass = gui_password

    api_overview_url = config.rabbit_mq_url + '/api/overview'
    response = rabbit_request(api_overview_url, 'GET')

    if response.reason == 'OK':
        print("Valid Credentials. Environment: ", config.environment)
        return True
    elif response.reason == 'Not Authorized':
        print("Invalid Credentials. Environment: ", config.environment)
        return False
    else:
        print("Connection Not Successful. Response: ", response.reason)
        return False


@eel.expose
def get_parameter(gui_parameter):
    """This function retrieves the requested parameter to the GUI"""

    if not type(gui_parameter) == str:
        print('Invalid Requested parameter: "', gui_parameter, '" -> Not a String')
        return "Invalid"

    if gui_parameter == 'environment':
        print('returning ', gui_parameter, ' = ', config.environment, ' to GUI')
        return config.environment

    if gui_parameter == 'DeviceID':
        print('returning ', gui_parameter, ' = ', config.DeviceID, ' to GUI')
        return config.DeviceID

    if gui_parameter == 'DeviceIP':
        print('returning ', gui_parameter, ' = ', config.DeviceIP, ' to GUI')
        return config.DeviceIP

    if gui_parameter == 'RFID':
        print('returning ', gui_parameter, ' = ', config.RFID, ' to GUI')
        return config.RFID

    if gui_parameter == 'fw_current':
        print('returning ', gui_parameter, ' = ', config.fw_current, ' to GUI')
        return config.fw_current

    if gui_parameter == 'fw_update':
        print('returning ', gui_parameter, ' = ', config.fw_update, ' to GUI')
        return config.fw_update

    if gui_parameter == 'vhost':
        print('returning ', gui_parameter, ' = ', config.vhost, ' to GUI')
        return config.vhost

    if gui_parameter == 'request_domain':
        print('returning ', gui_parameter, ' = ', config.request_domain, ' to GUI')
        return config.request_domain

    if gui_parameter == 'gps_lat':
        print('returning ', gui_parameter, ' = ', config.gps_lat, ' to GUI')
        return config.gps_lat

    if gui_parameter == 'gps_long':
        print('returning ', gui_parameter, ' = ', config.gps_long, ' to GUI')
        return config.gps_long

    if gui_parameter == 'timezone':
        print('returning ', gui_parameter, ' = ', config.timezone, ' to GUI')
        return config.timezone

    if gui_parameter == 'burn_hour_timeout':
        print('returning ', gui_parameter, ' = ', config.burn_hour_timeout, ' to GUI')
        return config.burn_hour_timeout

    if gui_parameter == 'temp_time_hyst':
        print('returning ', gui_parameter, ' = ', config.temp_time_hyst, ' to GUI')
        return config.temp_time_hyst

    if gui_parameter == 'temp_max':
        print('returning ', gui_parameter, ' = ', config.temp_max, ' to GUI')
        return config.temp_max

    if gui_parameter == 'start_up_seq_time':
        print('returning ', gui_parameter, ' = ', config.start_up_seq_time, ' to GUI')
        return config.start_up_seq_time

    if gui_parameter == 'photocell_enabled':
        print('returning ', gui_parameter, ' = ', config.photocell_enabled, ' to GUI')
        return config.photocell_enabled

    if gui_parameter == 'sensor_count':
        print('returning ', gui_parameter, ' = ', config.sensor_count, ' to GUI')
        return config.sensor_count

    if gui_parameter in config.ACPower_errors_dict:
        print('returning ', gui_parameter, ' = ', config.ACPower_errors_dict.get(gui_parameter), ' to GUI')
        return config.ACPower_errors_dict.get(gui_parameter)

    if gui_parameter == 'BrokenLamp':
        print('returning ', gui_parameter, ' = ', config.BrokenLamp, ' to GUI')
        return config.BrokenLamp

    if gui_parameter == 'DALIBallastStatus':
        print('returning ', gui_parameter, ' = ', config.DALIBallastStatus, ' to GUI')
        return config.DALIBallastStatus

    if gui_parameter in config.BurningHours_dict:
        print('returning ', gui_parameter, ' = ', config.BurningHours_dict.get(gui_parameter), ' to GUI')
        return config.BurningHours_dict.get(gui_parameter)

    if gui_parameter in config.dimfeedback_dict:
        print('returning ', gui_parameter, ' = ', config.dimfeedback_dict.get(gui_parameter), ' to GUI')
        return config.dimfeedback_dict.get(gui_parameter)

    if gui_parameter == 'num_imported_devices':
        print('returning ', gui_parameter, ' = ', len(config.imported_devices_list), ' to GUI')
        return len(config.imported_devices_list)

    if gui_parameter == 'sensor_type':
        print('returning ', gui_parameter, ' = ', config.sensor_type, ' to GUI')
        return config.sensor_type

    if gui_parameter == 'source_device_id':
        print('returning ', gui_parameter, ' = ', config.source_device_id, ' to GUI')
        return config.source_device_id

    if gui_parameter == 'sensor_hold_time':
        print('returning ', gui_parameter, ' = ', config.sensor_hold_time, ' to GUI')
        return config.sensor_hold_time

    if gui_parameter == 'sup_dim_feed':
        print('returning ', gui_parameter, ' = ', config.sup_dim_feed, ' to GUI')
        return config.sup_dim_feed

    print('Requested Parameter not found: ', gui_parameter)
    return "Not Found"


@eel.expose
def set_parameter(gui_parameter, gui_value):
    """
    This function receives the parameter to be set and the respective value as strings
    The Runtime Variables in config.py are updated herein
    Returns False if the input parameters are not valid
    """
    # Input Parameter must always be string
    if not type(gui_parameter) == str:
        print('Invalid Input Parameter: "', gui_parameter, '" -> discarding ....')
        return False
    # Input Value must always be either string or Boolean
    if type(gui_value) != str and type(gui_value) != bool:
        print('Invalid Input Value: "', gui_value, '" -> discarding ....')
        return False

    # Different parameters require different validations:

    # Check if it's the DeviceID
    if gui_parameter == "DeviceID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:    # Length Must be 16
            print('Invalid Device ID length: "', gui_value, '" -> discarding ....')
            return False
        elif not gui_value.isalnum():   # Must be Alphanumerical
            print('Invalid Device ID string: "', gui_value, '" -> discarding ....')
            return False
        else:
            config.DeviceID = gui_value
            print('Setting DeviceID ', config.DeviceID, ' from GUI')
            return True

    # Check if it's the DeviceIP
    if gui_parameter == "DeviceIP":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) > 12:     # Are IPs ever longer than 12?
            print('Invalid Device IP length: "', gui_value, '" -> discarding ....')
            return False
        # elif not gui_device_ip.isalnum():
        #     print('Invalid Device IP string: "', gui_device_ip, '" -> discarding ....')
        #     return False

        config.DeviceIP = gui_value
        print('Setting DeviceIP ', config.DeviceIP, ' from GUI')
        return True

    # Check if it's the RFID
    if gui_parameter == "RFID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:  # Length Must be 16
            print('Invalid RFID length: "', gui_value, '" -> discarding ....')
            return False
        elif not gui_value.isalnum():  # Must be Alphanumerical
            print('Invalid RFID string: "', gui_value, '" -> discarding ....')
            return False

        config.RFID = gui_value
        print('Setting RFID ', config.RFID, ' from GUI')
        return True

    # Check if it's the fw_current
    if gui_parameter == "fw_current":
        if len(gui_value) > 12:     # Are Firmware versions ever longer than 12?
            print('Invalid Current Firmware length: "', gui_value, '" -> discarding ....')
            return False
        config.fw_current = gui_value
        print('Setting fw_current ', config.fw_current, ' from GUI')
        return True

    # Check if it's the fw_update
    if gui_parameter == "fw_update":
        if len(gui_value) > 12:  # Are Firmware versions ever longer than 12?
            print('Invalid Firmware Update length: "', gui_value, '" -> discarding ....')
            return False
        config.fw_update = gui_value
        print('Setting fw_update ', config.fw_update, ' from GUI')
        return True

    # Check if it's the gps_lat
    if gui_parameter == "gps_lat":
        # Need to verify the input values. Return False if NOK
        try:
            f_gui_lat = float(gui_value)
        except ValueError:
            print('Invalid GPS Latitude: "', gui_value, '" -> discarding ....')
            return False

        # round to 6 decimal places
        f_gui_lat = round(f_gui_lat, 6)
        # set as a string
        config.gps_lat = str(f_gui_lat)
        print('Setting gps_lat ', config.gps_lat, ' from GUI')
        return True

    # Check if it's the gps_long
    if gui_parameter == "gps_long":
        # Need to verify the input values. Return False if NOK
        try:
            f_gui_long = float(gui_value)
        except ValueError:
            print('Invalid GPS Longitude: "', gui_value, '" -> discarding ....')
            return False

        # round to 6 decimal places
        f_gui_long = round(f_gui_long, 6)
        # set as a string
        config.gps_long = str(f_gui_long)
        print('Setting gps_long ', config.gps_long, ' from GUI')
        return True

    # Check if it's the timezone
    if gui_parameter == "timezone":
        # Need to verify the input values. Return False if NOK
        try:
            int_timezone = int(gui_value)
        except ValueError:
            print('Invalid Timezone: "', gui_value, '" -> discarding ....')
            return False

        if int_timezone < -12 or int_timezone > 14:
            print('Invalid Timezone: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.timezone = gui_value
        print('Setting timezone ', config.timezone, ' from GUI')
        return True

    # Check if it's the burn_hour_timeout
    if gui_parameter == "burn_hour_timeout":
        # Need to verify the input values. Return False if NOK
        try:
            int_burn_hour_timeout = int(gui_value)
        except ValueError:
            print('Invalid Burning Hours Timeout: "', gui_value, '" -> discarding ....')
            return False

        if int_burn_hour_timeout < 0:
            print('Invalid Burning Hours Timeout: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.burn_hour_timeout = gui_value
        print('Setting Burning Hours Timeout ', config.burn_hour_timeout, ' from GUI')
        return True

    # Check if it's the temp_time_hyst
    if gui_parameter == "temp_time_hyst":
        # Need to verify the input values. Return False if NOK
        try:
            int_temp_time_hyst = int(gui_value)
        except ValueError:
            print('Invalid Temperature Time Hysteresis: "', gui_value, '" -> discarding ....')
            return False

        if int_temp_time_hyst < 0:
            print('Invalid Temperature Time Hysteresis: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.temp_time_hyst = gui_value
        print('Setting Temperature Time Hysteresis ', config.temp_time_hyst, ' from GUI')
        return True

    # Check if it's the temp_max
    if gui_parameter == "temp_max":
        # Need to verify the input values. Return False if NOK
        try:
            int_temp_max = int(gui_value)
        except ValueError:
            print('Invalid Maximum Temperature: "', gui_value, '" -> discarding ....')
            return False

        if int_temp_max < 0:
            print('Invalid Maximum Temperature: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.temp_max = gui_value
        print('Setting Maximum Temperature ', config.temp_max, ' from GUI')
        return True

    # Check if it's the start_up_seq_time
    if gui_parameter == "start_up_seq_time":
        # Need to verify the input values. Return False if NOK
        try:
            int_start_up_seq_time = int(gui_value)
        except ValueError:
            print('Invalid Startup Sequence Time: "', gui_value, '" -> discarding ....')
            return False

        if int_start_up_seq_time < 0:
            print('Invalid Startup Sequence Time: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.start_up_seq_time = gui_value
        print('Setting Startup Sequence Time ', config.start_up_seq_time, ' from GUI')
        return True

    # Check if it's the photocell_enabled
    if gui_parameter == "photocell_enabled":
        # Need to verify the input string. Return False if NOK
        if gui_value == 'true' or gui_value == 'false':
            config.photocell_enabled = gui_value
            print('Setting Startup Sequence Time ', config.photocell_enabled, ' from GUI')
            return True
        else:
            print('Invalid Photocell Enabled value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's the sensor_count
    if gui_parameter == "sensor_count":
        # Need to verify the input values. Return False if NOK
        try:
            int_sensor_count = int(gui_value)
        except ValueError:
            print('Invalid Sensor Counts: "', gui_value, '" -> discarding ....')
            return False

        if int_sensor_count < 0:
            print('Invalid Sensor Counts: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.sensor_count = gui_value
        print('Setting Sensor Counts ', config.sensor_count, ' from GUI')
        return True

    # Check if it's the vhost
    if gui_parameter == "vhost":
        # Need to verify the input string. Return False if NOK
        if not gui_value.isalnum():  # Must be Alphanumerical
            print('Invalid Virtual Host string: "', gui_value, '" -> discarding ....')
            return False

        config.vhost = gui_value
        print('Setting vhost ', config.vhost, ' from GUI')
        return True

    # Check if it's the request_domain
    if gui_parameter == "request_domain":
        # Need to verify the input string. Return False if NOK
        if ".owlet-iot.com" in gui_value:
            config.request_domain = gui_value
            print('Setting request_domain ', config.request_domain, ' from GUI')
            return True

        print('Invalid Request Domain string: "', gui_value, '" -> discarding ....')
        return False

    # Check if it's an ACPower_error
    if gui_parameter in config.ACPower_errors_dict:
        if type(gui_value) == bool:
            # update the dictionary with the new value
            upd_param = {gui_parameter: gui_value}
            config.ACPower_errors_dict.update(upd_param)
            print('Setting ACPower_error ', gui_parameter, ' = ', config.ACPower_errors_dict[gui_parameter])
            return True
        else:
            print('Invalid ACPower_error Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a BrokenLamp_Error
    if gui_parameter == 'BrokenLamp':
        if type(gui_value) == bool:
            config.BrokenLamp = gui_value
            return True
        else:
            print('Invalid BrokenLamp Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a DALIBallastStatus_Error
    if gui_parameter == 'DALIBallastStatus':
        if gui_value == 'LampArcPowerOn' or gui_value == 'CommsError':
            config.DALIBallastStatus = gui_value
            print('Setting DALIBallastStatus to ', config.DALIBallastStatus, ' from GUI')
            return True
        else:
            print('Invalid DALIBallastStatus Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's the sensor_type
    if gui_parameter == 'sensor_type':
        if gui_value == 'local' or gui_value == 'OW3':
            config.sensor_type = gui_value
            print('Setting Sensor Type to ', config.sensor_type, ' from GUI')
            return True
        else:
            print('Invalid Sensor Type Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's the source_device_id
    if gui_parameter == 'source_device_id':
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:  # Length Must be 16
            print('Invalid Device ID length: "', gui_value, '" -> discarding ....')
            return False
        elif not gui_value.isalnum():  # Must be Alphanumerical
            print('Invalid Device ID string: "', gui_value, '" -> discarding ....')
            return False
        else:
            config.source_device_id = gui_value
            print('Setting Source Device ID to ', config.source_device_id, ' from GUI')
            return True

    # Check if it's the sensor_hold_time
    if gui_parameter == "sensor_hold_time":
        # Need to verify the input values. Return False if NOK
        try:
            int_sensor_hold_time = int(gui_value)
        except ValueError:
            print('Invalid Sensor Hold Time: "', gui_value, '" -> discarding ....')
            return False

        if int_sensor_hold_time < 0:
            print('Invalid Sensor Hold Time: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.sensor_hold_time = gui_value
        print('Setting Sensor Hold Time ', config.sensor_hold_time, ' from GUI')
        return True

    # Check if it's the sup_dim_feed
    if gui_parameter == 'sup_dim_feed':
        if gui_value == 'true' or gui_value == 'false':
            config.sup_dim_feed = gui_value
            print('Setting Suppress Dimfeedback to ', config.sup_dim_feed, ' from GUI')
            return True
        else:
            print('Invalid Suppress Dimfeedback Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a BurningHours value
    if gui_parameter in config.BurningHours_dict:
        # Need to verify the input values. Return False if NOK
        try:
            int_gui_value = int(gui_value)
        except ValueError:
            print('Invalid Burning Hours Value: "', gui_value, '" -> discarding ....')
            return False

        if int_gui_value < 0:
            print('Invalid Burning Hours Value: "', gui_value, '" -> discarding ....')
            return False

        # convert back to string
        gui_value = str(int_gui_value)
        # update the dictionary with the new value
        upd_param = {gui_parameter: gui_value}

        config.BurningHours_dict.update(upd_param)
        print('Setting Burning Hours ', gui_parameter, ' = ', config.BurningHours_dict[gui_parameter])
        return True

    # Check if it's a Dimfeedback value
    if gui_parameter in config.dimfeedback_dict:
        # Need to verify the input values. Return False if NOK
        try:
            # Check if it's convertible to float
            f_gui_value = float(gui_value)
        except ValueError:
            print('Invalid Dimfeedback Value: "', gui_value, '" -> discarding ....')
            return False

        # round to 3 decimal places
        f_gui_value = round(f_gui_value, 3)
        # convert back to string
        gui_value = str(f_gui_value)
        # update the dictionary with the new value
        upd_param = {gui_parameter: gui_value}

        config.dimfeedback_dict.update(upd_param)
        print('Setting Dimfeedback ', gui_parameter, ' = ', config.dimfeedback_dict[gui_parameter])
        return True

    # if none of the above, means the parameter does not exist
    print('Unknown Input values:  Parameter: ', gui_parameter, ';  Value: ', gui_value)
    return False


@eel.expose
def send_d2c_coap_together(queue_list, b_is_single_device, device_list=[]):
    """
    This Function receives a list of parameters that shall be sent in the via Coap, like:
    ['BurningHours', 'Dimfeedback_1','EnergyMeter']
    Encapsulates all in one queue and sends a single Coap Message
    If it's a single device (b_is_single_device), the target is the device defined in Settings
    If it's not a single device (b_is_single_device), the targets are the devices in the received List
    Returns False if no parameters are received, if they are invalid,
    or if the Coap message is not successfully sent
    """

    debug_num_devices = 0

    if len(queue_list) == 0:
        print("Queue is empty. Check input parameters. Coap message not Sent.")
        return False

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"
    print("Post Request URL: ", post_request_url)

    if b_is_single_device:
        # build the queue
        queue = build_g3_d2c_queue(queue_list, config.DeviceID)
        # build the payload
        message_body = build_g3_d2c_message(queue, config.DeviceID, config.DeviceIP)

        # encapsulate the G3 message body inside the request payload
        payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

        # send the request
        print("Sending Device 2 Cloud Message with:\n", queue_list)

        response = rabbit_request(post_request_url, 'POST', payload)
        # print('\n put_request_url response: ', response)

        if response.status_code != 200:
            print("Response Status Code:", response)
            return False

        print('Coap message with ', queue_list, ' sent successfully! Routed: ', response.content)

    else:  # Not a single device, so send the message from every device on the list
        for device in device_list[0]:
            # build the queue
            queue = build_g3_d2c_queue(queue_list, device.get("Device_ID"))
            # build the payload
            message_body = build_g3_d2c_message(queue, device.get("Device_ID"), device.get("IP address"))

            # encapsulate the G3 message body inside the request payload
            payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

            # send the request
            response = rabbit_request(post_request_url, 'POST', payload)
            if response.status_code != 200:
                return False
            else:
                debug_num_devices += 1

    if not b_is_single_device:
        print("Successfully sent message to ", debug_num_devices, " devices")

    return True


@eel.expose
def send_d2c_coap_separately(queue_list, b_is_single_device, device_list=[]):
    """
    This Function receives a list of parameters that shall be sent in the via Coap, like:
    ['BurningHours', 'Dimfeedback_1','EnergyMeter']
    It calls send_d2c_coap_together for every parameter separately
    Returns False if no parameters are received or if any Coap message is not successfully sent
    """

    if len(queue_list) == 0:
        print("Queue is empty. Check input parameters. Coap message not Sent.")
        return False

    # Iterate through all elements in the received queue_list and call send_d2c_coap_together() for each one
    for parameter in queue_list:
        parameter_as_list = [parameter]
        send_ok = send_d2c_coap_together(parameter_as_list, b_is_single_device, device_list)
        if not send_ok:
            print("Error Sending Coap Message with ", parameter)
            return False

    return True


@eel.expose
def send_d2c_coap_from_csv(queue_list, b_together):
    """
    This Function is triggered when the user selects "Send to All Devices" in the Device to Cloud MEnu
    It creates a fixed number of threads, creating equally distributed lists and each thread calls
    send_d2c_coap_together() or send_d2c_coap_separately() with a list of devices
    Returns "True" when all the threads are finished
    """

    # need to separate the full device list through the threads
    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append(
            [config.imported_devices_list[devices_copied: (devices_copied + devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    # send_d2c_coap_together(queue_list, False, list_of_lists[thread])
    # send_d2c_coap_separately(queue_list, False, list_of_lists[thread])

    # Create the threads
    for thread in range(num_threads):
        if b_together:
            new_thread = threading.Thread(target=send_d2c_coap_together,
                                          args=(queue_list, False, list_of_lists[thread]))
        else:
            new_thread = threading.Thread(target=send_d2c_coap_separately,
                                          args=(queue_list, False, list_of_lists[thread]))

        # append each thread handler to the threads_list
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return True


@eel.expose
def send_c2d_coap(b_is_single_device, device_list=[]):
    """
    This Function sends a Cloud to Device Message
    If it's a single device (b_is_single_device), the target is the device defined in Settings
    If it's not a single device (b_is_single_device), the targets are the devices in the received List
    By default, the device list is an Empty List, unless stated otherwise (b_is_single_device == False)
    Returns False if if the response is not 200 - OK
    """

    debug_num_devices = 0

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldOutbox/publish"

    if b_is_single_device:
        # build the payload
        coap_payload = create_coap_payload(selected_c2d_message, config.DeviceID)
        message_body = build_g3_c2d_message(coap_payload, config.DeviceID, config.DeviceIP)

        # encapsulate the G3 message body inside the request payload
        payload = '{"properties":{},"routing_key":"worldOutbox","payload":"' + message_body + '","payload_encoding":"string"}'

        # send the request
        response = rabbit_request(post_request_url, 'POST', payload)
        print('\n3 put_request_url response:\n', response, response.content)

        if response.status_code != 200:
            print("Response Status Code:", response)
            return False

        print('Coap message with ', selected_c2d_message, ' sent successfully! Routed: ', response.content)

    else:  # Not a single device, so send the message from every device on the list
        for device in device_list[0]:
            coap_payload = create_coap_payload(selected_c2d_message, device.get("Device_ID"))
            message_body = build_g3_c2d_message(coap_payload, device.get("Device_ID"), device.get("IP address"))

            # encapsulate the G3 message body inside the request payload
            payload = '{"properties":{},"routing_key":"worldOutbox","payload":"' + message_body + '","payload_encoding":"string"}'

            # send the request
            response = rabbit_request(post_request_url, 'POST', payload)
            if response.status_code != 200:
                return False
            else:
                debug_num_devices += 1

    if not b_is_single_device:
        print("Successfully sent message to ", debug_num_devices, " devices")

    return True


@eel.expose
def send_c2d_coap_from_csv():
    """
    This Function is triggered when the user selects "Send to All Devices" in the Cloud to Device Menu
    It creates a fixed number of threads, creating equally distributed lists and each thread calls
    send_c2d_coap() with a list of devices
    Returns "True" when all the threads are finished
    """

    # need to separate the full device list through the threads
    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append(
            [config.imported_devices_list[devices_copied: (devices_copied + devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    #send_c2d_coap(False, list_of_lists[thread])

    # Create the threads
    for thread in range(num_threads):

        new_thread = threading.Thread(target=send_c2d_coap,
                                      args=(False, list_of_lists[thread]))
        # append each thread handler to the threads_list
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return True


@eel.expose
def set_c2d_message(message):
    """
    This function sets the current Cloud to Device message selected by the user
    Returns False if the Selected message is not a valid string
    Returns True otherwise
    """

    global selected_c2d_message

    # Input Parameter must always be string
    if not type(message) == str or len(message) == 0:
        print('Invalid Input message: "', message, '" -> discarding ....')
        return False

    selected_c2d_message = message
    print('Selected Cloud 2 Device Message: ', selected_c2d_message)

    return True


@eel.expose
def send_c2d_custom_coap(b_is_single_device, g3_object, g3_index, custom_payload, device_list=[]):
    """
    This Function sends a Cloud to Device Custom Message
    If it's a single device (b_is_single_device), the target is the device defined in Settings
    If it's not a single device (b_is_single_device), the targets are the devices in the received List
    By default, the device list is an Empty List, unless stated otherwise (b_is_single_device == False)
    The g3_object, g3_index and custom_payload must be passed when calling this function
    Returns False if if the response is not 200 - OK
    """

    debug_num_devices = 0

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldOutbox/publish"

    coap_payload = [g3_object, g3_index, custom_payload]

    if b_is_single_device:
        # build the payload
        # coap_payload = create_coap_payload(selected_c2d_message, config.DeviceID)
        message_body = build_g3_c2d_message(coap_payload, config.DeviceID, config.DeviceIP)

        # encapsulate the G3 message body inside the request payload
        payload = '{"properties":{},"routing_key":"worldOutbox","payload":"' + message_body + '","payload_encoding":"string"}'

        # send the request
        response = rabbit_request(post_request_url, 'POST', payload)
        print('\n3 put_request_url response:\n', response, response.content)

        if response.status_code != 200:
            print("Response Status Code:", response)
            return False

        print('Coap message with ', selected_c2d_message, ' sent successfully! Routed: ', response.content)

    else:  # Not a single device, so send the message from every device on the list
        for device in device_list[0]:
            # coap_payload = create_coap_payload(selected_c2d_message, device.get("Device_ID"))
            message_body = build_g3_c2d_message(coap_payload, device.get("Device_ID"), device.get("IP address"))

            # encapsulate the G3 message body inside the request payload
            payload = '{"properties":{},"routing_key":"worldOutbox","payload":"' + message_body + '","payload_encoding":"string"}'

            # send the request
            response = rabbit_request(post_request_url, 'POST', payload)
            if response.status_code != 200:
                return False
            else:
                debug_num_devices += 1

    if not b_is_single_device:
        print("Successfully sent message to ", debug_num_devices, " devices")

    return True


@eel.expose
def send_c2d_custom_coap_from_csv(g3_object, g3_index, custom_payload):
    """
    This function is used to send the Custom Payload from the Web UI to all the devices loaded from the CSV file
    It creates the threads and splits the devices among them
    Each thread runs send_c2d_custom_coap() with a list of devices
    """

    # need to separate the full device list through the threads
    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append(
            [config.imported_devices_list[devices_copied: (devices_copied + devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    #send_c2d_custom_coap(False, g3_object, g3_index, custom_payload, list_of_lists[thread])

    # Create the threads
    for thread in range(num_threads):

        new_thread = threading.Thread(target=send_c2d_custom_coap,
                                      args=(False, g3_object, g3_index, custom_payload, list_of_lists[thread]))
        # append each thread handler to the threads_list
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return True


@eel.expose
def get_c2d_payload():
    """
    This function retrieves the payload of the currently selected message "selected_c2d_message" to the Web UI
    """

    payload = create_coap_payload(selected_c2d_message, config.DeviceID)

    return payload[2]

@eel.expose
def change_environment(b_production):
    """
    This function changes the current environment to production, changing the required Runtime Variables
    Returns True if change was successful
    Returns False otherwise
    """

    if b_production:
        config.environment = "production"
        config.rabbit_mq_url = config.rabbit_mq_prd_url
        # This is just for test purposes. request domain shall be user input!
        config.request_domain = config.request_domain_prd
        # This is just for test purposes. Virtual host shall be user input!
        config.vhost = config.vhost_prd
        config.ow_rconf_url = config.ow_rconf_prod_url
        config.exedra_Rurl = config.exedra_Rurl_prd
        config.exedra_Surl = config.exedra_Surl_prd
        print('Setting Environment to', config.environment)
    else:
        config.environment = "test"
        config.rabbit_mq_url = config.rabbit_mq_stage_url
        # This is just for test purposes. request domain shall be user input!
        config.request_domain = config.request_domain_stage
        # This is just for test purposes. Virtual host shall be user input!
        config.vhost = config.vhost_stage_lisbon
        config.ow_rconf_url = config.ow_rconf_stage_url
        config.exedra_Rurl = config.exedra_Rurl_tst
        config.exedra_Surl = config.exedra_Surl_tst
        print('Setting Environment to', config.environment)

    return True


@eel.expose
def save_current_settings():
    """
    This function saves the current environment settings to the settings.json file,
    calling config.save_current_settings() from config.py module.
    Returns True if save is successful
    Returns False if an error occurred
    """

    return config.save_current_settings()


if __name__ == '__main__':

    config.load_settings()

    # Start the GUI
    start_eel()

    # Code shall never reach this section as it will remain in a loop inside start_eel()
    # Some simple GET requests to test the API request function and the authentication
    ##################################################################
    api_overview_url = config.rabbit_mq_url + '/api/overview'
    response = rabbit_request(api_overview_url, 'GET')
    print('\n api overview:\n', response.text)

    api_vhosts_list_url = config.rabbit_mq_url + '/api/vhosts'
    response = rabbit_request(api_overview_url, 'GET')
    print('\n vhosts list:\n', response.text)

    vhost_queues_url = config.rabbit_mq_url + '/api/queues/' + config.vhost
    response = rabbit_request(vhost_queues_url, 'GET')
    print('\n lisbon queues list:\n', response.text)

    ##################################################################
