//Functions herein are dedicated to Saving or Loading Settings from Python

async function saveSettings_device2cloudMenu()
    {
<!--    Set Energy Meter      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("energy_meter");

        <!--    Pass its value to Python function    -->
        let energy_meter_ok = await eel.set_parameter( "EnergyMeter", element.value )();

        if (energy_meter_ok != true) {  <!--    Not OK From Python    -->
            console.log('EnergyMeter Setting Unsuccessful. Python returned ' + energy_meter_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set ACPower      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("ac_power");

        <!--    Pass its value to Python function    -->
        let ac_power_ok = await eel.set_parameter( "ACPower", element.value )();

        if (ac_power_ok != true) {  <!--    Not OK From Python    -->
            console.log('ACPower Setting Unsuccessful. Python returned ' + ac_power_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set ACCurrent      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("ac_current");

        <!--    Pass its value to Python function    -->
        let ac_current_ok = await eel.set_parameter( "ACCurrent", element.value )();

        if (ac_current_ok != true) {  <!--    Not OK From Python    -->
            console.log('ACCurrent Setting Unsuccessful. Python returned ' + ac_current_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set ACVoltage      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("ac_voltage");

        <!--    Pass its value to Python function    -->
        let ac_voltage_ok = await eel.set_parameter( "ACVoltage", element.value )();

        if (ac_voltage_ok != true) {  <!--    Not OK From Python    -->
            console.log('ACVoltage Setting Unsuccessful. Python returned ' + ac_voltage_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set ACPowerFactor      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("ac_pwr_factor");

        <!--    Pass its value to Python function    -->
        let ac_pwr_factor_ok = await eel.set_parameter( "ACPowerFactor", element.value )();

        if (ac_pwr_factor_ok != true) {  <!--    Not OK From Python    -->
            console.log('ACPowerFactor Setting Unsuccessful. Python returned ' + ac_pwr_factor_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set FeedbackDIMLevel      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("feedback_dim_level");

        <!--    Pass its value to Python function    -->
        let feedback_dim_level_ok = await eel.set_parameter("FeedbackDIMLevel", element.value )();

        if (feedback_dim_level_ok != true) {  <!--    Not OK From Python    -->
            console.log('FeedbackDIMLevel Setting Unsuccessful. Python returned ' + feedback_dim_level_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set MinACPower      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("min_ac_power");

        <!--    Pass its value to Python function    -->
        let min_ac_power_ok = await eel.set_parameter( "MinACPower", element.value )();

        if (min_ac_power_ok != true) {
            console.log('MinACPower Setting Unsuccessful. Python returned ' + min_ac_power_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set MaxACPower      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("max_ac_power");

        <!--    Pass its value to Python function    -->
        let max_ac_power_ok = await eel.set_parameter( "MaxACPower", element.value )();

        if (max_ac_power_ok != true) {
            console.log('MaxACPower Setting Unsuccessful. Python returned ' + max_ac_power_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set TotalControllerRuntime      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("total_controller_runtime");

        <!--    Pass its value to Python function    -->
        let total_controller_runtime_ok = await eel.set_parameter( "TotalControllerRuntime", element.value )();

        if (total_controller_runtime_ok != true) {
            console.log('TotalControllerRuntime Setting Unsuccessful. Python returned ' + total_controller_runtime_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set TotalRuntime      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("total_runtime");

        <!--    Pass its value to Python function    -->
        let total_runtime_ok = await eel.set_parameter( "TotalRuntime", element.value )();

        if (total_runtime_ok != true) {
            console.log('TotalRuntime Setting Unsuccessful. Python returned ' + total_runtime_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set ErrPWRPF      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrPWRPF_checkbox");
        console.log('ErrPWRPF_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrPWRPF_ok = await eel.set_parameter( "ErrPWRPF", element.checked )();

        if (ErrPWRPF_ok != true) {
            console.log('ErrPWRPF Setting Unsuccessful. Python returned ' + ErrPWRPF_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set ErrVoltageHigh      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrVoltageHigh_checkbox");
        console.log('ErrVoltageHigh_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrVoltageHigh_ok = await eel.set_parameter( "ErrVoltageHigh", element.checked )();

        if (ErrVoltageHigh_ok != true) {
            console.log('ErrVoltageHigh Setting Unsuccessful. Python returned ' + ErrVoltageHigh_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set ErrZXMissing      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrZXMissing_checkbox");
        console.log('ErrZXMissing_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrZXMissing_ok = await eel.set_parameter( "ErrZXMissing", element.checked )();

        if (ErrZXMissing_ok != true) {
            console.log('ErrZXMissing Setting Unsuccessful. Python returned ' + ErrZXMissing_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set ErrPWRHigh      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrPWRHigh_checkbox");
        console.log('ErrPWRHigh_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrPWRHigh_ok = await eel.set_parameter( "ErrPWRHigh", element.checked )();

        if (ErrPWRHigh_ok != true) {
            console.log('ErrPWRHigh Setting Unsuccessful. Python returned ' + ErrPWRHigh_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set ErrVoltageLow      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrVoltageLow_checkbox");
        console.log('ErrVoltageLow_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrVoltageLow_ok = await eel.set_parameter( "ErrVoltageLow", element.checked )();

        if (ErrVoltageLow_ok != true) {
            console.log('ErrVoltageLow Setting Unsuccessful. Python returned ' + ErrVoltageLow_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set ErrSAGDetected      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrSAGDetected_checkbox");
        console.log('ErrSAGDetected_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrSAGDetected_ok = await eel.set_parameter( "ErrSAGDetected", element.checked )();

        if (ErrSAGDetected_ok != true) {
            console.log('ErrSAGDetected Setting Unsuccessful. Python returned ' + ErrSAGDetected_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set ErrPWRLow      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrPWRLow_checkbox");
        console.log('ErrPWRLow_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrPWRLow_ok = await eel.set_parameter( "ErrPWRLow", element.checked )();

        if (ErrPWRLow_ok != true) {
            console.log('ErrPWRLow Setting Unsuccessful. Python returned ' + ErrPWRLow_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set ErrNoLoad      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("ErrNoLoad_checkbox");
        console.log('ErrNoLoad_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let ErrNoLoad_ok = await eel.set_parameter( "ErrNoLoad", element.checked )();

        if (ErrNoLoad_ok != true) {
            console.log('ErrNoLoad Setting Unsuccessful. Python returned ' + ErrNoLoad_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set BrokenLamp      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("BrokenLamp_checkbox");
        console.log('BrokenLamp_checkbox is :' + element.checked);

        <!--    Pass its value to Python function    -->
        let BrokenLamp_ok = await eel.set_parameter( "BrokenLamp", element.checked )();

        if (BrokenLamp_ok != true) {
            console.log('BrokenLamp Setting Unsuccessful. Python returned ' + BrokenLamp_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }

<!--    Set DALIBallastStatus      -->
        <!--    Get the Checkbox element    -->
        var element = document.getElementById("DALIBallastStatus_checkbox");
        console.log('DALIBallastStatus_checkbox is :' + element.checked);

        var status;


        <!--    Pass its value to Python function    -->
        if (element.checked) {
            status = "CommsError";
        }
        else {
            status = "LampArcPowerOn";
        }
        let DALIBallastStatus_ok = await eel.set_parameter( "DALIBallastStatus", status )();

        if (DALIBallastStatus_ok != true) {
            console.log('DALIBallastStatus Setting Unsuccessful. Python returned ' + DALIBallastStatus_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#5B9BD5';
        }


<!--    Save Settings to File      -->
        var btn = document.getElementById("save_settings");

        let save_ok = await eel.save_current_settings()();

        if (save_ok != true) {
            await sleep (1000);
            btn.value = "Save NOK!";
            btn.style.backgroundColor= '#F44336';
            await sleep (2000);
            btn.value = "Save";
            btn.style.backgroundColor= '#5B9BD5';

        }
        else {
            btn.value = "Save OK";
            btn.style.backgroundColor= '#4CAF50';
            await sleep (2000);
            btn.value = "Save";
            btn.style.backgroundColor= '#5B9BD5';
        }
    }

async function saveSettings_settingsMenu()
    {

<!--    Set DeviceID      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("DeviceID");

        <!--    Pass its value to Python function    -->
        let device_id_ok = await eel.set_parameter( "DeviceID", element.value )();

        if (device_id_ok != true) {  <!--    Not OK From Python    -->
            console.log('DeviceID Setting Unsuccessful. Python returned ' + device_id_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set DeviceIP      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("DeviceIP");

        <!--    Pass its value to Python function    -->
        let device_ip_ok = await eel.set_parameter( "DeviceIP", element.value )();

        if (device_ip_ok != true) {  <!--    Not OK From Python    -->
            console.log('DeviceIP Setting Unsuccessful. Python returned ' + device_ip_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set RFID      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("RFID");

        <!--    Pass its value to Python function    -->
        let rfid_ok = await eel.set_parameter( "RFID", element.value )();

        if (rfid_ok != true) {  <!--    Not OK From Python    -->
            console.log('RFID Setting Unsuccessful. Python returned ' + rfid_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Firmware Current      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("fw_current");

        <!--    Pass its value to Python function    -->
        let fw_current_ok = await eel.set_parameter( "fw_current", element.value )();

        if (fw_current_ok != true) {  <!--    Not OK From Python    -->
            console.log('Firmware Current Setting Unsuccessful. Python returned ' + fw_current_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Firmware Update      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("fw_update");

        <!--    Pass its value to Python function    -->
        let fw_update_ok = await eel.set_parameter( "fw_update", element.value )();

        if (fw_current_ok != true) {  <!--    Not OK From Python    -->
            console.log('Firmware Update Setting Unsuccessful. Python returned ' + fw_update_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Latitude      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("gps_lat");

        <!--    Pass its value to Python function    -->
        let lat_ok = await eel.set_parameter( "gps_lat", element.value )();

        if (lat_ok != true) {  <!--    Not OK From Python    -->
            console.log('Latitude Setting Unsuccessful. Python returned ' + lat_ok);
            <!--    Set Textbox to Red    -->
            element.style.backgroundColor = '#F44336';
        }
        else {   <!--    OK From Python    -->
        <!--    Set Textbox to Gray    -->
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Longitude      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("gps_long");

        <!--    Pass its value to Python function    -->
        let long_ok = await eel.set_parameter( "gps_long", element.value )();

        if (long_ok != true) {
            console.log('Longitude Setting Unsuccessful. Python returned ' + long_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Timezone      -->
        <!--    Get the Select Box element    -->
        var element = document.getElementById("timezone_selectbox");

        <!--    Pass its value to Python function    -->
        let timezone_ok = await eel.set_parameter( "timezone", element.value )();

        if (timezone_ok != true) {
            console.log('Timezone Setting Unsuccessful. Python returned ' + timezone_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Burning Hours Timeout      -->
        <!--    Get the Select Box element    -->
        var element = document.getElementById("burn_hour_timeout");

        <!--    Pass its value to Python function    -->
        let burn_hour_timeout_ok = await eel.set_parameter( "burn_hour_timeout", element.value )();

        if (burn_hour_timeout_ok != true) {
            console.log('Burning Hours Setting Unsuccessful. Python returned ' + burn_hour_timeout_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Temperature Time Hysteresis      -->
        <!--    Get the Select Box element    -->
        var element = document.getElementById("temp_time_hyst");

        <!--    Pass its value to Python function    -->
        let temp_time_hyst_ok = await eel.set_parameter( "temp_time_hyst", element.value )();

        if (temp_time_hyst_ok != true) {
            console.log('Temperature Time Hysteresis Setting Unsuccessful. Python returned ' + temp_time_hyst_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Maximum Temperature      -->
        <!--    Get the Select Box element    -->
        var element = document.getElementById("temp_max");

        <!--    Pass its value to Python function    -->
        let temp_max_ok = await eel.set_parameter( "temp_max", element.value )();

        if (temp_max_ok != true) {
            console.log('Temperature Time Hysteresis Setting Unsuccessful. Python returned ' + temp_max_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Startup Sequence Time      -->
        <!--    Get the Select Box element    -->
        var element = document.getElementById("start_up_seq_time");

        <!--    Pass its value to Python function    -->
        let start_up_seq_time_ok = await eel.set_parameter( "start_up_seq_time", element.value )();

        if (start_up_seq_time_ok != true) {
            console.log('Startup Sequence Time Setting Unsuccessful. Python returned ' + start_up_seq_time_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Photocell Enabled/Disabled      -->
        <!--    Get the Select Box element    -->
        var element = document.getElementById("photocell_enabled");

        <!--    Pass its value to Python function    -->
        let photocell_enabled_ok = await eel.set_parameter( "photocell_enabled", element.value )();

        if (photocell_enabled_ok != true) {
            console.log('Photocell Setting Unsuccessful. Python returned ' + photocell_enabled_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Sensor Counts      -->
        <!--    Get the Select Box element    -->
        var element = document.getElementById("sensor_count");

        <!--    Pass its value to Python function    -->
        let sensor_count_ok = await eel.set_parameter( "sensor_count", element.value )();

        if (sensor_count_ok != true) {
            console.log('Sensor Counts Setting Unsuccessful. Python returned ' + sensor_count_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Virtual Host      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("vhost");

        <!--    Pass its value to Python function    -->
        let vhost_ok = await eel.set_parameter( "vhost", element.value )();

        if (vhost_ok != true) {
            console.log('Virtual Host Setting Unsuccessful. Python returned ' + vhost_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Set Request Domain      -->
        <!--    Get the Textbox element    -->
        var element = document.getElementById("request_domain");

        <!--    Pass its value to Python function    -->
        let req_domain_ok = await eel.set_parameter( "request_domain", element.value )();

        if (req_domain_ok != true) {
            console.log('Request Domain Setting Unsuccessful. Python returned ' + req_domain_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

<!--    Save Settings to File      -->
        var btn = document.getElementById("save_settings");

        let save_ok = await eel.save_current_settings()();

        if (save_ok != true) {
            await sleep (1000);
            btn.value = "Save NOK!";
            btn.style.backgroundColor= '#F44336';
            await sleep (2000);
            btn.value = "Save";
            btn.style.backgroundColor= '#5B9BD5';

        }
        else {
            btn.value = "Save OK";
            btn.style.backgroundColor= '#4CAF50';
            await sleep (2000);
            btn.value = "Save";
            btn.style.backgroundColor= '#5B9BD5';
        }
    }

async function loadCSVfile()
    {
        var btn = document.getElementById("load_csv_button");
        var filename = document.getElementById("csv_filename");
        var loaded_devices_txt = document.getElementById("loaded_devices");

        let load_ok = await eel.read_devices_from_csv(csv_filename.value)();

        if (load_ok != true) {
            await sleep (1000);
            btn.value = "Load NOK!";
            btn.style.backgroundColor= '#F44336';
            await sleep (2000);
            btn.value = "Load";
            btn.style.backgroundColor= '#5B9BD5';

        }
        else {
            btn.value = "Load OK";
            btn.style.backgroundColor= '#4CAF50';
            let loaded_devices_num = await eel.get_parameter('num_imported_devices')();
            loaded_devices_txt.innerHTML = loaded_devices_num + ' devices loaded';
            await sleep (2000);
            btn.value = "Load";
            btn.style.backgroundColor= '#5B9BD5';
        }

    }

    async function setParameter(element)
    {
    var set_parameter_ok = await eel.set_parameter(element.id, element.value )();

        if (set_parameter_ok != true) {
            console.log('Setting ' + element.id + ' Unsuccessful. Python returned ' + set_parameter_ok);
            element.style.backgroundColor = '#F44336';
        }
        else {
            element.style.backgroundColor = '#D0CECE';
        }

    }