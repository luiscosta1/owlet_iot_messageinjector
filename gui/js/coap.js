//Functions herein are dedicated to sending the Coap Requests to Python

async function sendCoap(btn)
    {
<!-- Go through the "Message Sending" checkboxes to evaluate which Messages shall be sent    -->
        var list = [];
        var element;

        element = document.getElementById("send_verification_checkbox");
        if (element.checked) { list.push("Verification"); }

        element = document.getElementById("send_lampreg_checkbox");
        if (element.checked) { list.push("LampReg"); }

        element = document.getElementById("send_ACPower_errors_checkbox");
        if (element.checked) {
          list.push("ErrPWRPF");
          list.push("ErrVoltageHigh");
          list.push("ErrZXMissing");
          list.push("ErrPWRHigh");
          list.push("ErrVoltageLow");
          list.push("ErrSAGDetected");
          list.push("ErrPWRLow");
          list.push("ErrNoLoad"); }

        element = document.getElementById("send_lamp_errors_1_checkbox");
        if (element.checked) { list.push("BrokenLamp_1"); list.push("DALIBallastStatus_1");}

        element = document.getElementById("send_lamp_errors_2_checkbox");
        if (element.checked) { list.push("BrokenLamp_2"); list.push("DALIBallastStatus_2"); }

        element = document.getElementById("send_dimfeedback_1_checkbox");
        if (element.checked) { list.push("Dimfeedback_1"); }

        element = document.getElementById("send_dimfeedback_2_checkbox");
        if (element.checked) { list.push("Dimfeedback_2"); }

        element = document.getElementById("send_energy_readings_checkbox");
        if (element.checked) { list.push("EnergyReading"); }

        element = document.getElementById("send_burning_hours_checkbox");
        if (element.checked) { list.push("BurningHours"); }

        element = document.getElementById("send_position_update_checkbox");
        if (element.checked) { list.push("PositionUpdate"); }

        element = document.getElementById("send_sensor_count_checkbox");
        if (element.checked) { list.push("SensorCount"); }

        console.log('List of Parameters to send in Coap: ' + list);

<!-- Send Message All in one or in Separate commands    -->
        let send_ok = false;

        if (btn.id == "send_together"){
        <!--    Send Together from 1 device   -->
            send_ok = await eel.send_d2c_coap_together(list,true)();
        }
        else if (btn.id == "send_separately") {
        <!--    Send Separately from 1 device  -->
            send_ok = await eel.send_d2c_coap_separately(list,true)();
        }
        else if (btn.id == "send_together_list"){
        <!--    Send Separately from device list. true means together   -->
            send_ok = await eel.send_d2c_coap_from_csv(list, true)();
        }
        else if (btn.id == "send_separately_list"){
        <!--    Send Separately from device list.  false means separately   -->
            send_ok = await eel.send_d2c_coap_from_csv(list, false)();
        }

<!-- Provide user feedback, changing the button color  -->
        if (send_ok != true) {
            btn.style.backgroundColor= '#F44336';
            await sleep (2000);
            btn.style.backgroundColor= '#5B9BD5';
        }
        else {
            btn.style.backgroundColor= '#4CAF50';
            await sleep (2000);
            btn.style.backgroundColor= '#5B9BD5';
        }

    }

async function sendCoap2Device(btn)
    {
    <!-- Send Message All in one or in Separate commands    -->
        send_ok = false;

        if (btn.id == "send_one_device"){
        <!--    Send Together to 1 device   -->
            send_ok = await eel.send_c2d_coap(true)();
        }
        else if (btn.id == "send_multiple_devices") {
        <!--    Send Separately to CSV device list  -->
            send_ok = await eel.send_c2d_coap_from_csv()();
        }

<!-- Provide user feedback, changing the button color  -->
        if (send_ok != true) {
            btn.style.backgroundColor= '#F44336';
            await sleep (2000);
            btn.style.backgroundColor= '#5B9BD5';
        }
        else {
            btn.style.backgroundColor= '#4CAF50';
            await sleep (2000);
            btn.style.backgroundColor= '#5B9BD5';
        }

    }

async function sendCustomCoap2Device(btn)
    {
    <!-- Send Message All in one or in Separate commands    -->
        send_ok = false;
        object = document.getElementById("custom_object").value;
        index = document.getElementById("custom_index").value;
        payload = document.getElementById("message_preview").value;

        if (btn.id == "send_custom_one_device"){
        <!--    Send Together to 1 device   -->
            send_ok = await eel.send_c2d_custom_coap(true, object, index, payload)();
        }
        else if (btn.id == "send_custom_multiple") {
        <!--    Send Separately to CSV device list  -->
            send_ok = await eel.send_c2d_custom_coap_from_csv(object, index, payload)();
        }

<!-- Provide user feedback, changing the button color  -->
        if (send_ok != true) {
            btn.style.backgroundColor= '#F44336';
            await sleep (2000);
            btn.style.backgroundColor= '#5B9BD5';
        }
        else {
            btn.style.backgroundColor= '#4CAF50';
            await sleep (2000);
            btn.style.backgroundColor= '#5B9BD5';
        }

    }