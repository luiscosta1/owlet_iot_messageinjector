####################################################################################
# Filename:     g3_c2d_message_builder.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20.04.2021
# Last update:  23.04.2021
#
# Version:      2.0
#
# Filetype:     Function file
# Description:  Functions required for G3 C2D message building
# STATUS:       Stable
# Limitation:
####################################################################################


def build_g3_c2d_message(coap, deviceID, deviceIP):
    """
    This function receives the desired information Queue and encapsulates it into the G3 message body template
    Returns the full message body to be sent on the payload to Rabbit MQ
    """

    g3_message_body = '{ ' \
        '  "url": "coap://' + deviceIP + ':4096/' + coap[0] + '/' + coap[1] + '",' \
        '  "method": "put",' \
        '  "payload":  ' \
                + coap[2] + \
        '    ,    ' \
        '  "__type":      "ctrl.OwletMessageInjectorTool", ' \
        '  "__server":    "project", ' \
        '  "__deviceId":"' + deviceID + '" ' \
        ' }'
    # add escaping backslash
    g3_message_body = g3_message_body.replace('/', '\\/')
    # add escaping "
    g3_message_body = g3_message_body.replace('"', '\\"')  # OK

    return g3_message_body

