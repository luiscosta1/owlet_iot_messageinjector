####################################################################################
# Filename:     test_send_g3_C2D_messages.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   21.04.2021
# Last update:  27.04.2021
#
# Version:      2.0
#
# Filetype:     Test case file
# Description:  Tests Sending Owlet IoT to G3 Device messages
# STATUS:       Stable
# Limitation:
####################################################################################

from main import *
from coap import *
from csv_devices import *
import threading


def test_send_g3_dali_reinit():

    message_body = build_g3_c2d_message(coap_dali_reinit, config.DeviceID, config.DeviceIP)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldOutbox/publish"

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"worldOutbox","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_debug_info():

    message_body = build_g3_c2d_message(coap_debug_info, config.DeviceID, config.DeviceIP)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldOutbox/publish"

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"worldOutbox","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_debug_info_to_devices_list():

    read_devices_from_csv('DeviceAssetView_Lisbon.csv') # saves to config.imported_devices_list[]

    # TODO: Multi-DALI devices will show up as duplicate in the list. Should we remove them?

    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append([config.imported_devices_list[ devices_copied : (devices_copied+devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    # send_c2d_coap(list_of_lists[thread], 'debug_info')

    # Create the threads
    for thread in range(num_threads):
        new_thread = threading.Thread(target=send_c2d_coap, args=(list_of_lists[thread], 'debug_info'))
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return
