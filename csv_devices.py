####################################################################################
# Filename:     csv_devices.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   25.04.2021
# Last update:  25.04.2021
#
# Version:      2.0
#
# Filetype:     Function file
# Description:  Functions required for reading the device list from the CSV file
# STATUS:       Stable
# Limitation:
####################################################################################

import csv
import config
import eel


@eel.expose
def read_devices_from_csv(filename):
    """
    This function receives the full path of the csv file and saves them to a list in config.py
    Returns False if the file is not found
    """

    config.imported_devices_list = []
    try:
        with open(filename, newline='') as file:
            reader = csv.DictReader(file)
            for row in reader:
                config.imported_devices_list.append(row)

    except FileNotFoundError:
        print("File not Found. Exiting")
        return False

    print(len(config.imported_devices_list), " Devices imported:")

    for device in config.imported_devices_list:
        print('Device_ID:', device["Device_ID"])

    return True
